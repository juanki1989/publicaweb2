<?php

/**
 * @file
 * Contains field_library test.
 */

/**
 * A test class for field_library functionality.
 */
class FieldLibraryTest extends FieldUITestCase {

  /**
   * @var \StdClass
   * The admin user.
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Field library',
      'description' => 'Test the field library module functionality.',
      'group' => 'Field Library',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp(array('field_library', 'ctools', 'field_ui', 'node'));
    $this->adminUser = $this->drupalCreateUser(array(
      'administer field_library',
      'use field_library',
      'administer nodes',
      'bypass node access',
      'administer content types',
    ));
  }

  /**
   * Test the field library functionality.
   */
  public function testFieldLibrary() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/structure/field-library/field_library_bundle');
    $this->clickLink('Add');
    $this->drupalPost(NULL, array(
      'name' => 'first_bundle',
      'title' => 'First bundle',
      'description' => 'My first bundle',
    ), t('Save'));
    $this->assertText('first_bundle has been created');
    $this->assertText('First bundle');
    $this->clickLink('Manage fields');
    $edit = array(
      'fields[_add_new_field][label]' => 'Test field',
      'fields[_add_new_field][field_name]' => 'test',
    );
    $this->fieldUIAddNewField('admin/structure/field-library/manage/first_bundle', $edit);
    $field = field_info_field('field_test');
    $this->assertNotNull($field);

    $instance = field_info_instance('field_library', 'field_test', 'first_bundle');
    $this->assertNotNull($instance);

    $this->drupalCreateContentType(array('type' => 'test', 'name' => 'Test type'));
    $this->drupalGet('admin/structure/types/manage/test/fields');
    $this->assertFieldByName('field_library_bundle');
    $this->drupalPost(NULL, array(
      'field_library_bundle' => 'first_bundle',
    ), t('Add from library'));
    $this->assertText('You are about to add the following fields to the Test type bundle of the Node entity type.');
    $this->assertText('Test field');
    $this->drupalPost(NULL, array(), t('Continue'));
    $this->assertText('Added the Test field field to the Test type bundle of the Node entity type.');
    // Check that the field appears in the overview form.
    $this->drupalGet('admin/structure/types/manage/test/fields');
    $this->assertFieldByXPath('//table[@id="field-overview"]//td[1]', 'Test field', 'Field was created and appears in the overview page.');
  }

}
